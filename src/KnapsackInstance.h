#ifndef KNAPSACKINSTANCE_H_
#define KNAPSACKINSTANCE_H_

#include <cmath>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <limits>
#include <vector>
#include <cassert>
#include <set>
#include <utility>

#include "KnapsackItem.h"
#include "KnapsackSolution.h"

//Represents an instance of the knapsack problem that can be loaded from files and solved
//using different approximation algorithms.
class KnapsackInstance 
{
    public:
        //Reads an instance from file
        static KnapsackInstance* readFromFile(const char* file);

        //Computes a 2-factor approximation
        KnapsackSolution approximate();
        //Computes a (1+eps)-factor approximation
        KnapsackSolution approximateEps();
        //Uses dynamic programming to solve the knapsack problem exactly given an upper 
        //bound on the total profit
        KnapsackSolution dynamicProgramming(unsigned int C);
        unsigned int opt(unsigned int C);

    private:
        KnapsackInstance() {}

        //Remove items weighing more than the weight limit W
        void processItems();
        //Scales the profits of all items by a given factor
        void scale(const double t);

        //Number of items in the instance
        unsigned int n;
        //Number of items in the instance that weigh less than W
        unsigned int n_processed;
        //The maximum total weight
        unsigned int W;
        //epsilon
        double eps;
        //All items given in the instance
        std::vector<KnapsackItem> items;
        //All items given in the instance that do not weigh more than W
        std::vector<KnapsackItem> items_processed;

    friend std::ostream& operator<<(std::ostream& os, const KnapsackInstance* inst);
};

//Checks whether a given item exceeds the weight limit
struct IsTooHeavyFunctor
{
    public:
        IsTooHeavyFunctor(unsigned int W) : _W(W)
        {
        }
        
        bool operator()(const KnapsackItem& item)
        {
            return item.w > _W;
        }

    private:
        unsigned int _W;
};

//Scales the profit of an item by a given factor
struct ScaleFunctor
{
    public:
        ScaleFunctor(double t) : _t(t)
        {
        }
        
        KnapsackItem operator()(const KnapsackItem& item)
        {
            KnapsackItem newItem = item;
            newItem.c = (unsigned int) std::floor(item.c / _t);
            return newItem;
        }

    private:
        double _t;
};

std::ostream& operator<<(std::ostream& os, const KnapsackInstance* inst);

#endif
