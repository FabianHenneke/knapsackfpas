#include <iostream>
#include "KnapsackInstance.h"
#include "KnapsackSolution.h"

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " instance\n";
		return 1;
	}

    KnapsackInstance* instance = KnapsackInstance::readFromFile(argv[1]);

    if (instance == NULL)
        return 1;
    //std::cout << instance;
    //std::cout << instance;
    //std::cout << "2-factor approximation: " << instance->approximate().computeProfit() <<std::endl;
    KnapsackSolution sol = instance->approximateEps();
    std::cout << sol;
    //std::cout << "approximate:            " << sol;
    //std::cout << "approximate profit:     " << sol.computeProfit()<< std::endl;

    //std::cout << "OPT:                    " << instance->opt(2*instance->approximate().computeProfit()) << std::endl;

    delete instance;

    //std::cout << instance;

    return 0;
}
