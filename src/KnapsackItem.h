#ifndef KNAPSACKITEM_H_
#define KNAPSACKITEM_H_

/*
Represents an item, having its weight and profit as properties.
*/
struct KnapsackItem
{
    unsigned int id;
    unsigned int w;
    unsigned int c;

    //Compares two items by id
    bool operator<(const KnapsackItem& other) const;
};

//Compares the ratio of profit to weight of two items, used in the 2-factor approximation
struct ProfitWeightRatioComparator
{
	bool operator() (const KnapsackItem& item1, const KnapsackItem& item2)
	{
    	return (item1.c*item2.w > item2.c*item1.w); 
	}
};

//Used to add up the profits of several items in a solution to the knapsack problem
struct AddProfitFunctor
{
	unsigned int operator() (unsigned int profit, const KnapsackItem& item)
	{
		return profit + item.c;
	}
};

#endif 
