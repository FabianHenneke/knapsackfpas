#include "KnapsackItem.h"

//Compares two items by id
bool KnapsackItem::operator<(const KnapsackItem& other) const
{
    return (id < other.id); 
}

