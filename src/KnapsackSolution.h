#ifndef KNAPSACKSOLUTION_H_
#define KNAPSACKSOLUTION_H_

#include <vector>
#include <numeric>
#include <iostream>
#include <algorithm>

#include "KnapsackItem.h"

//Contains the items used in a solution for the knapsack problem
class KnapsackSolution 
{
    public:
    	//Computes the total profit of all items in the solution
    	unsigned int computeProfit();
    	//Adds one item to the solution
    	void addItem(const KnapsackItem& item);
    	//Sorts the items by id in ascending order
    	void sort();

    private: 
    	std::vector<KnapsackItem> items; 

    friend std::ostream& operator<<(std::ostream& os, KnapsackSolution& sol);
};

std::ostream& operator<<(std::ostream& os, KnapsackSolution& sol);


#endif
