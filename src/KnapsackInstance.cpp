#include "KnapsackInstance.h"

KnapsackInstance* KnapsackInstance::readFromFile(const char* file)
{
    std::ifstream in(file);

    if (!in)
    {
        std::cout << "Could not open file " << file << "\n";
        return NULL;
    }

    KnapsackInstance* inst = new KnapsackInstance();
    
    unsigned int i;

    in >> inst->n;
    in >> inst->W;
    in >> inst->eps;

    for (i = 0; i < inst->n; i++)
    {
        KnapsackItem item;
        item.id = i;    //id
        in >> item.c;   //profit
        in >> item.w;   //weight

        inst->items.push_back(item);
    }

    return inst;
}

void KnapsackInstance::processItems()
{
    IsTooHeavyFunctor isTooHeavy(W);
    //Compute number of items that are not too heavy
    items_processed.resize(n - std::count_if(items.begin(), items.end(), isTooHeavy));

    //Copy all items to items_processed that are not too heavy
    std::remove_copy_if(items.begin(), items.end(), items_processed.begin(), isTooHeavy);
    n_processed = items_processed.size();
}

void KnapsackInstance::scale(const double t)
{
    if (t <= 0)
        return;

    std::transform(items_processed.begin(), items_processed.end(), items_processed.begin(), ScaleFunctor(t));
}

KnapsackSolution KnapsackInstance::approximate()
{
    //Remove items that weigh too much
    processItems();
    //Sort items by their profit-weight-ratio in decreasing ratio
    std::sort(items_processed.begin(), items_processed.end(), ProfitWeightRatioComparator());    

    unsigned int k = 0;
    unsigned int accWeight = 0;

    //Solution containing the first k items that fit
    KnapsackSolution sol1;
    //Solution containing just the (k+1)st item
    KnapsackSolution sol2;

    //Just take items (having highest profit-weight-ratio) until the knapsack is full
    for (k = 0; k < n_processed; k++)
    {
        if (accWeight + items_processed[k].w <= W)
        {
            accWeight += items_processed[k].w;
            sol1.addItem(items_processed[k]);
        }
        else
        {
            break;
        }
    }

    //If all items fit, this is an optimum solution
    if (k == n_processed)
        return sol1;

    //Just take the next item
    sol2.addItem(items_processed[k]);

    //Return the better solution of the two
    if (sol1.computeProfit() < sol2.computeProfit())
        return sol2;
    else 
        return sol1;
}

KnapsackSolution KnapsackInstance::approximateEps()
{
    //Compute 2-factor approximation
    KnapsackSolution sol1 = approximate();

    //Scale the instance according to the choice of eps
    unsigned int cS1 = sol1.computeProfit();
    double t = std::max(1.0, (eps * cS1) / n_processed);
    scale(t);

    //Solve the instance by dynamic programming
    KnapsackSolution sol2 = dynamicProgramming((unsigned int) std::floor(2*cS1/t) + 1);

    //Return the solution that yields higher profit
    if (cS1 > sol2.computeProfit())
        return sol1;      
    else
        return sol2;
}

unsigned int KnapsackInstance::opt(unsigned int C)
{
    unsigned int i, j, k;
    
    unsigned int** x = new unsigned int*[n+1];
    for (j = 0; j < n+1; j++)
    {
        x[j] = new unsigned int[C+1];
    }

    x[0][0] = 0;
    for (k = 1; k <= C; k++)
    {
        x[0][k] = std::numeric_limits<unsigned int>::max();
    }

    for (j = 1; j <= n; j++)
    {
        for (k = 0; k <= C; k++)
        {
            x[j][k] = x[j-1][k];
        }
        for (k = items[j-1].c; k <= C; k++)
        {
            if (x[j-1][k-items[j-1].c] != std::numeric_limits<unsigned int>::max() 
                && x[j-1][k-items[j-1].c] + items[j-1].w <= std::min(W, x[j][k]))
            {
                x[j][k] = x[j-1][k-items[j-1].c] + items[j-1].w;
            }
        }
    }

    unsigned int best = 0;
    for (k = 0; k <= C; k++)
    {
        if (x[n][k] < std::numeric_limits<unsigned int>::max())
            best = k;
    }

    unsigned int profit = 0;
        std::vector<unsigned int> solution;


    for (j = n; j >= 1; j--)
    {
        if (best >= items[j-1].c
            && x[j-1][best-items[j-1].c] != std::numeric_limits<unsigned int>::max() 
            && x[j][best] == x[j-1][best-items[j-1].c] + items[j-1].w)
        {
            solution.push_back(j-1);
            profit += items[j-1].c;
            best -= items[j-1].c;
        }
    }

    assert(best == 0);

    std::sort(solution.begin(), solution.end());

    for (i = 0; i < solution.size(); i++)
    {
        std::cout << solution[i];
        if (i == solution.size() - 1)
            std::cout << '\n';
        else 
            std::cout << ' ';
    }

    for (j = 0; j < n + 1; j++)
    {
        delete[] x[j];
    }
    delete[] x;

    return profit;
}

KnapsackSolution KnapsackInstance::dynamicProgramming(unsigned int C)
{
    unsigned int j, k;
    //unsigned int c1;

    //std::cout << "[dynamicProgramming] n_processed = " << n_processed << '\n';
    //std::cout << "[dynamicProgramming] C           = " << C << '\n';
    //std::cout << "[dynamicProgramming] Memory consumption: " << C * n_processed / (1024.0*1024.0*1024.0) << " GB\n";

    //Allocate an array of size (n+1)*(C+1)
    unsigned int** x = new unsigned int*[n_processed+1];
    for (j = 0; j < n_processed+1; j++)
    {
        x[j] = new unsigned int[C+1];
    }

    //Initialize x
    x[0][0] = 0;
    for (k = 1; k <= C; k++)
    {
        x[0][k] = std::numeric_limits<unsigned int>::max();
    }

    //c1 = 0;
    //Loop over all items
    for (j = 1; j <= n_processed; j++)
    {
        /*c1 = (c1 + 1) % ((n_processed / 10) + 1);
        if (c1 == 0)
        {
            std::cout << "[dynamicProgramming] n_processed: " << j << "/" << n_processed << "\n";
            std::cin >> i;
        }*/
        //Copy values (Case: new item is not used)
        for (k = 0; k <= C; k++)
        {
            x[j][k] = x[j-1][k];
        }
        //Compute new values (Case: new item is used)
        for (k = items_processed[j-1].c; k <= C; k++)
        {
            //Check whether profit is possible to obtain (value != max)
            if (x[j-1][k-items_processed[j-1].c] != std::numeric_limits<unsigned int>::max() 
                && x[j-1][k-items_processed[j-1].c] + items_processed[j-1].w <= std::min(W, x[j][k]))
            {
                x[j][k] = x[j-1][k-items_processed[j-1].c] + items_processed[j-1].w;
            }
        }
    }

    //Compute best profit possible
    unsigned int best = 0;
    for (k = 0; k <= C; k++)
    {
        if (x[n_processed][k] < std::numeric_limits<unsigned int>::max())
            best = k;
    }

    KnapsackSolution sol;

    //Obtain s implicitly from x and add items to the solution
    for (j = n_processed; j >= 1; j--)
    {
        if (best >= items_processed[j-1].c
            && x[j-1][best-items_processed[j-1].c] != std::numeric_limits<unsigned int>::max() 
            && x[j][best] == x[j-1][best-items_processed[j-1].c] + items_processed[j-1].w)
        {
            sol.addItem(items[items_processed[j-1].id]);
            best -= items_processed[j-1].c;
        }
    }

    //Sum of profits of the items should be the best obtainable profit
    assert(best == 0);

    //Release memory
    for (j = 0; j < n_processed; j++)
    {
        delete[] x[j];
    }
    delete[] x;

    return sol;
}

std::ostream& operator<<(std::ostream& os, const KnapsackInstance* inst)
{
    unsigned int i;

    if (inst == NULL)
        return os;

    if (inst->n != 0 && inst->items.size() == 0)
    {
        os << "Items unitialized\n";
        return os;
    }

    os << "Instance: n=" << inst->n << ", W=" << inst->W << ", eps=" << inst->eps << "\n";

    for (i = 0; i < inst->n; i++)
    {
        os << "Item " << i << ": " << inst->items[i].w << " " << inst->items[i].c << "\n";
    }

    return os;
}
