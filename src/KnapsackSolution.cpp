#include "KnapsackSolution.h"

//Computes the total profit of all items in the solution
unsigned int KnapsackSolution::computeProfit()
{
	return std::accumulate(items.begin(), items.end(), 0, AddProfitFunctor());
}

//Adds one item to the solution
void KnapsackSolution::addItem(const KnapsackItem& item)
{
	items.push_back(item);
}

//Sorts the items by id in ascending order
void KnapsackSolution::sort()
{
	std::sort(items.begin(), items.end());
}


std::ostream& operator<<(std::ostream& os, KnapsackSolution& sol)
{
	sol.sort();

	unsigned int i;

	for (i = 0; i < sol.items.size(); i++)
	{
		os << sol.items[i].id;

		if (i == sol.items.size() - 1)
			os << '\n';
		else 
			os << ' ';
	}

	return os;
}
